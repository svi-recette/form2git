Feature: Connexion JDD
Scenario Outline: se connecter sur l application jpetstore
    Given je suis sur la page de connexion jpetstore
    When je renseigne mon login "<login>"
    And je renseigne le mot de passe "<mdp>"
    Then Lutilisateur "<user>" est connecte

    
Examples: 
      | login  | mdp | user  |
      | j2ee |j2ee | ABC |
      | ACID |ACID | ABC |