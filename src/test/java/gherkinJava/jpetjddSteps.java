package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetjddSteps {
	
	WebDriver driver;
	
	@Given("je suis sur la page de connexion jpetstore")
	public void je_suis_sur_la_page_de_connexion_jpetstore() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/a[2]")).click();
	}
	
	
	@When("je renseigne mon login {string}")
	public void je_renseigne_mon_login(String login) {
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(login);
	}

	@When("je renseigne le mot de passe {string}")
	public void je_renseigne_le_mot_de_passe(String mdp) {
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(mdp);
		driver.findElement(By.name("signon")).click();
	}
	
	@Then("Lutilisateur {string} est connecte")
	public void lutilisateur_est_connecte(String user) {
		driver.findElement(By.xpath("//div[contains(text(),'Welcome " + user + "!')]")); 
		driver.close();
	}
}