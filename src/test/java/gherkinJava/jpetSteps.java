package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetSteps {
	
	WebDriver driver;

@Given("je suis sur la page jpetstore")
public void je_suis_sur_la_page_jpetstore() throws Throwable {
	System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
	driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
}

@Given("je suis sur la page de connexion")
public void je_suis_sur_la_page_de_connexion() {
    // Write code here that turns the phrase above into concrete actions
driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/a[2]")).click();

}

@When("je renseigne mon login j2ee")
public void je_renseigne_mon_login_j2ee() {
    // Write code here that turns the phrase above into concrete actions

	driver.findElement(By.name("username")).clear();
	driver.findElement(By.name("username")).sendKeys("j2ee");
	
}

@When("je renseigne le mot de passe j2ee")
public void je_renseigne_le_mot_de_passe_j2ee() {
    // Write code here that turns the phrase above into concrete actions
	driver.findElement(By.name("password")).clear();
	driver.findElement(By.name("password")).sendKeys("j2ee");
	driver.findElement(By.name("signon")).click();
}

@Then("Lutilisateur ABC est connecte")
public void lutilisateur_ABC_est_connecte() {
    // Write code here that turns the phrase above into concrete actions
	driver.findElement(By.xpath("//div[contains(text(),'Welcome ABC!')]")); 
}


}
